package com.springBoot.elasticsearch;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;

import java.util.Collections;

public class UpdateES {
    private final Logger LOGGER = LogManager.getLogger(UpdateES.class);
    RestHighLevelClient esClient;
    UpdateES(){
        String elasticHost = "localhost";
        int elasticPort = 9200;
        this.esClient = new RestHighLevelClient(RestClient.builder(
                new HttpHost(elasticHost, elasticPort,"http")));
    }

    public void update_ES(){
        UpdateByQueryRequest request = new UpdateByQueryRequest("air_messages");
        request.setConflicts("proceed");
        request.setBatchSize(10);
        request.setScroll(TimeValue.timeValueMinutes(10));
        request.setRefresh(true);
        request.setScript(
                new Script(
                        ScriptType.INLINE, "painless",
                        "if (ctx._source.userId.compareTo('10000') > 0) {ctx._source.source ='IN';}",
                        Collections.emptyMap()));
        ActionListener<BulkByScrollResponse> listener = new ActionListener<BulkByScrollResponse>() {
            @Override
            public void onResponse(BulkByScrollResponse bulkResponse) {
                LOGGER.info("Successfully Updated");
            }
            @Override
            public void onFailure(Exception e) {
                LOGGER.info("Failed updating with exception : " + e.toString());
            }
        };
        esClient.updateByQueryAsync(request,RequestOptions.DEFAULT,listener);
    }
}
